function disp(mtVar, iname)
% DISPLAY Display array (long).
%
% Example: m=MappedTensor(rand(10)); disp(m); 1

  if numel(mtVar) > 1
    display(mtVar, inputname(1));
    disp(mtVar(1), [ inputname(1) '(1)' ]);
    return;
  end
  
  strSize = strtrim(sprintf(' %d', size(mtVar)));

  if (mtVar.bIsComplex)
    strComplex = 'complex ';
  else
    strComplex = '';
  end

  if nargin < 2
    if ~isempty(inputname(1))
     iname = inputname(1);
    else
     iname = 'ans';
    end
  end

  display(mtVar, iname);

  disp([ '  Filename:   ' mtVar.Filename ' ' mtVar.FilenameCmplx ]);
  disp([ '  Name:       ' mtVar.Name ]);
  disp([ '  Writable:   ' mat2str(mtVar.Writable) ])
  disp([ '  Offset:     ' num2str(mtVar.Offset) ])
  disp([ '  Format:     ' mtVar.Format ])
  disp([ '  Data:       [' strtrim(sprintf(' %d', size(mtVar))) '] ' mtVar.Format ' array' ])
  disp([ '  Temporary:  ' mat2str(mtVar.Temporary) ])
  disp([ '  Bytes:      ' num2str(mtVar.Bytes) ]);
  disp(' ');
end
