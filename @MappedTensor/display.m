function display(mtVar, iname)
% DISPLAY Display array (short).
%
% Example: m=MappedTensor(rand(10)); disp(m); 1

  if nargin < 2,
    if ~isempty(inputname(1))
      iname = inputname(1);
    else
      iname = 'ans';
    end
  end

  sz     = size(mtVar);
  mtVar1 = mtVar; mtVar2 = mtVar;
  if numel(mtVar) > 1
    if ~isnumeric(sz)
      for index=1:numel(mtVar)
        display(mtVar(index), [ iname '(' num2str(index) ')' ]);
      end
      return
    else
      mtVar1 = mtVar(1);
      mtVar2 = mtVar(numel(mtVar));
    end
  end
  
  if any(~isreal(mtVar))
    strComplex = 'complex ';
  else
    strComplex = '';
  end
  if mtVar1.bCompressed
    strComplex = [ strComplex 'compressed ']; 
  end
  if numel(mtVar) > 1
    strComplex = [ strComplex 'stack ' ];
  end
  strSize = strtrim(sprintf(' %d', sz));

  if ~isdeployed
   disp(sprintf([ '%s = ' ...
     '<a href="matlab:help MappedTensor">MappedTensor</a>' ...
     ' %s%s [%s] (' ...
     '<a href="matlab:methods ' class(mtVar) '">methods</a>,' ...
     '<a href="matlab:doc(''' class(mtVar) ''')">doc</a>,' ...
     '<a href="matlab:subsref(' iname ', substruct(''()'', repmat({'':''}, 1, ndims(' iname '))))">values</a>,' ...
     '<a href="matlab:plot(' iname ')">plot</a>,' ... 
     '<a href="matlab:disp(' iname ');">more...</a>): %s' ], ...
     iname, strComplex, mtVar1.Format, strSize, mtVar1.Name)); %#ok<DSPS>
  else
   disp(sprintf('  %s = MappedTensor %s%s [%s]: %s', ...
     iname, strComplex, mtVar1.Format, strSize, mtVar1.Name));
  end

  % now display few elements from the array
  nb = 4;  % number of elements to display at beginning and end.
  sz1=size(mtVar1);
  sz2=size(mtVar2);
  if prod(sz1) < 8 || prod(sz2) < 8, nb=min(ceil(prod(sz1)/3), ceil(prod(sz2)/3)); end
  index1 = 1:nb;
  index2 = prod(sz2) + (-nb:0);
  index1 = index1(index1 <= prod(sz1));
  index2 = index2(1 <= index2);
  if isempty(index2)
    disp(subsref(mtVar1, substruct('()', {index1})));
  else
    s1 = subsref(mtVar1, substruct('()', {index1})); s1=s1(1:nb); s1=s1(:)';
    s2 = subsref(mtVar2, substruct('()', {index2})); s2=s2(1:nb); s2=s2(:)';
    disp([  '  ' num2str(s1) ' ... ' num2str(s2) ]);
  end
end
