function mtVar = load(mt0, filename)
% LOAD Lazy loading of data into a mapped tensor.
%   M = LOAD(MappedTensor, 'file.ext') loads the disk-mapped dataset (lazy loading)
%   Wildcards can be used to import a set of files, and return multiple mapped tensors.
%
%   M = LOAD(MappedTensor, {'file1','file2',...}) loads multiple data sets.
%
%   M = LOAD(MappedTensor, 'file.hdf5:/dataset/path') loads an HDF5 data set.
%   Only the real part is handled though (no complex data import).
%   Wildcards can be used to import a set of files, and return multiple mapped tensors.
%   Tokens and Regular expressions can be used to select the dataset, for instance
%   the exact path to a dataset, a word to match, or a full regular expression.
%   M = LOAD(MappedTensor, 'file.hdf5') returns an array of objects pointing to
%   all included numeric datasets.
%
%   M = LOAD(MappedTensor, 'file.mat:variable') loads a variable from MAT data set.
%   M = LOAD(MappedTensor, 'file.mat') returns an array of objects pointing to
%   all included numeric variables.
%
%   M = LOAD(MappedTensor, 'file.hdf5:/dataset/path?query') loads an HDF5 data set
%   applies a 'query' when searching for datasets/variables. This is especially useful when 
%   the dataset path is not set (e.g. widlcards or regex).
%
%   M = LOAD(MappedTensor, 'file.mat:variable?query') loads a MAT data set
%   using the same syntax as for HDF5 files.
%
%   The query follows URL standards and can be given with syntax:
%     ?attr         check for existence of attribute 'attr'
%     ?attr=val     check for existence and value of attribute 'attr'
%                   the value can be given as a string or numeric.
%                   for instance '?signal=1'
%                   other comparisons operators are possible with 
%                     '=','<=','>=','<','>','~='
%     ?attr.fun     applies given function 'fun' on attribute value and test for
%                   'true'. For instance ?signal.isnumeric
%                   additional arguments can be given, such as 
%                     ?attr.fun(arg1, ...) is equivalent to testing fun(attr, arg1, ...)
%                   for instance ?attr.gt(1) is the same as ?attr>1
%     ?attr.fun(arg)>val is a combinaison of previous allowed syntax, searching 
%                   for datasets/variables satisfying the condition.
%     ?query1&query2  test multiple conditions. All must be true to select a dataset.
%   Some attributes are pre-defined to ease dataset identification:
%     shape = size  holds the dataset size as [n1 n2 ...]
%     ndim  = ndims holds the dataset dimensionality (1,2,...)
%     len   = numel holds the dataset number of elements ('size' in NumPy)
%     dtype = class holds the dataset storage class (double, single, int16, ...)
%
%   Supported formats:
%    | Extension         | Description                                  |
%    |-------------------|----------------------------------------------|
%    | EDF EHF           | ESRF Data Format  (2D)                       |
%    | HDF5 H5 HDF NX NXS| Hierarchical Data Format (nD)                |
%    | IMG+HDR           | Analyze 7.5 Mayo Clinic (3D/4D)              |
%    | IMG SMV           | ADSC X-ray detector image SMV (2D)           |
%    | MAR MCCD          | MAR CCD image (2D)                           |
%    | MAT               | Matlab MAT 7.3 file (nD HDF5 based)          |
%    | MRC MAP CCP4 RES  | MRC MRC/CCP4/MAP electronic density map (3D) |
%    | NII               | NifTI-1 MRI volume data (nD)                 |
%    | NPY               | Python NumPy array (nD)                      |
%    | NRRD              | Nearly Raw Raster Data (nD)                  |
%    | POS               | Atom Probe Tomography  (4 columns)           |
%    | TIFF              | Uncompressed Tagged Image File Format (Nx2D) |
%    | VOL (PAR)         | PyHST2 tomography volume (3D)                |
%   
% Distant files are not handled (would require to e.g. retrieve them).
%
%    You may as well import an existing RAW binary file if you know the encoding
%    the size, and the offset (where binary data starts, e.g. after a header).
%
%    M = MappedTensor('Filename', 'file', 'Format', 'uint16', ...
%          'Size', 2000 2000 512], 'Offset', 0);
%

mtVar = [];

if nargin < 2, disp('not enough args'); return; end
if ~ischar(filename) && ~iscellstr(filename)
  error([ mfilename ': not a file (expect char/cellstr), but is ' class(filename) ]); 
end

% handle multiple files to load
if ischar(filename) && size(filename,1) > 1
  filename = cellstr(filename);
end

if iscell(filename) && numel(filename) > 1
  for index=1:numel(filename)
    mtVar = [ mtVar load(mt0, filename{index}) ];
  end
  return
end

% now single file --------------------------------------------------------------

if strncmp(filename, 'file://', length('file://'))
  filename = filename(7:end); % remove 'file://' from local name
end
% handle the '%20' character replacement as space (from URL), and others.
filename = strrep(filename, '%20',' ');
filename = strrep(filename, '+'  ,' ');
filename = strrep(filename, '%28','(');
filename = strrep(filename, '%29',')');
filename = strrep(filename, '%3C','<');
filename = strrep(filename, '%3E','>');
filename = strrep(filename, '%5B','[');
filename = strrep(filename, '%5D',']');
filename = strrep(filename, '%7B','{');
filename = strrep(filename, '%7D','}');

% handle HDF5 dataset path
[tf, filename, dataset] = mt_ishdf5(filename);

% handle single file name (possibly with wildcard)
if ~isempty(find(filename == '*')) | ~isempty(find(filename == '?'))  % wildchar !!#
  [filepath,name,ext]=fileparts(filename);  % 'file' to search
  if isempty(filepath), filepath = pwd; end
  this_dir = dir(filename);
  if isempty(this_dir), return; end % directory is empty
  % removes '.' and '..'
  index    = find(~strcmp('.', {this_dir.name}) & ~strcmp('..', {this_dir.name}));
  this_dir = char(this_dir.name);
  this_dir = (this_dir(index,:));
  if isempty(this_dir), return; end % directory only contains '.' and '..'
  rdir     = cellstr(this_dir); % original directory listing as cell
  rdir     = strcat([ filepath filesep ], char(rdir));
  filename = cellstr(rdir);
  mtVar = [ mtVar load(mt0, filename) ];
  return
end

% handle ~ substitution for $HOME
if filename(1) == '~' && (length(filename==1) || filename(2) == '/' || filename(2) == '\')
  filename(1) = '';
  if usejava('jvm')
    filename = [ char(java.lang.System.getProperty('user.home')) filename ];
  elseif ~ispc  % does not work under Windows
    filename = [ getenv('HOME') filename ];
  end
end

if isempty(dir(filename))
  error([ mfilename ': ERROR: Invalid filename ' filename ]);
end

% get file type / extension
[p,f,e] = fileparts(filename);

if strcmp(upper(e), 'IMG') && ( ~isempty(dir(fullfile(p, [f '.hdr' ]))) ...
  || ~isempty(dir(fullfile(p, [f '.HDR' ]))) )
  e = '.HDR';
end

% IMPORT -----------------------------------------------------------------------

switch upper(e)
case {'.EDF','.EHF'}
  [Descr,args,header] = private_load_edf(filename);
case '.IMG'
  % ADSC (IMG/SMV)
  [Descr,args,header] = private_load_smv(filename);
case {'.HDF','.HDF5','.H5','.NXS','.NX','.HE5'}
  [Descr,args,header] = private_load_hdf5(filename, dataset);
case '.HDR'
  [Descr,args,header] = private_load_analyze(filename);
  filename = fullfile(p, [f '.img' ]);
case {'.MAR','.MCCD'}
  [Descr,args,header] = private_load_mar(filename);
case '.MAT'
  [Descr,args,header] = private_load_mat(filename, dataset);
case {'.MRC','.MAP','.CCP4','.RES'}
  [Descr,args,header] = private_load_mrc(filename);
case '.NII' % NifTi
  [Descr,args,header] = private_load_nii(filename);
case '.NPY'
  [Descr,args,header] = private_load_npy(filename);
case '.NRRD'
  [Descr,args,header] = private_load_nrrd(filename);
case '.POS'
  [Descr,args,header] = private_load_pos(filename);
case '.SMV' % ADSC (IMG/SMV)
  [Descr,args,header] = private_load_smv(filename);
case {'.TIFF','.TIF'}
  [Descr,args,header] = private_load_tiff(filename);
case '.VOL'
  [Descr,args,header] = private_load_vol(filename);

otherwise
  error([ mfilename ': unsupported file type ' upper(e) ' for ' filename ]);
end

% ------------------------------------------------------------------------------


% each loader should specify the Offset, Format, MachineFormat, Dimensions

if ~iscell(Descr)
  Descr = { Descr }; args = { args }; header = { header };
end

mtVar = [];
for index=1:numel(Descr)

  % success: map file
  if ~isempty(Descr{index})
    if numel(Descr) ==1
      in = '';
    else
      in = [ num2str(index) '/' num2str(numel(Descr)) ' ' ];
    end
    
    if mt_verbosity>0
      disp([ mfilename ': lazy loading ' filename ' ' Descr{index} ' ' in ]);
    end
    A = args{index};
    isfilename = find(strcmpi(A(1:2:end), 'Filename'));
    if isempty(isfilename)
      mt = MappedTensor('Filename', filename, A{:});
    else
      mt = MappedTensor(A{:});
      filename = A{2*isfilename};
    end

    mt.Temporary = false;
    mt.Writable  = false; % protect data
    mt.UserData  = header{index};
    if any(strcmp(upper(e), {'.HDF','.HDF5','.H5','.NX','.NXS','.HE5'})) % must include dataset
      mt.Filename  = filename;
      mt.Name      = [ filename ':' Descr{index} ' HDF5 file ' in ];
      mt.H5_dataset= Descr{index};
    elseif strcmp(upper(e), '.MAT') % must include dataset
      mt.Filename  = filename;
      mt.Name      = [ filename ':' Descr{index} ' MAT file ' in ];
      mt.MAT_dataset= Descr{index};
      mt.Writable  = true;
    else
      mt.Name      = [ filename ':' Descr{index} ' ' in ];
    end
    mtVar = [ mtVar mt ];
  end
end

