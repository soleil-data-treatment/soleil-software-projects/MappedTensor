function [nBytes, strStorageClass, Format] = ClassSize(Format)
    
    % treat aliases to data storage classes
    switch upper(Format)
    case {'FLOAT','FLOAT32','H5T_IEEE_F32LE','H5T_IEEE_F32BE', ...
      'H5T_UNIX_D32BE','H5T_UNIX_D32LE','H5T_IEEE_F32BE','H5T_IEEE_F32LE'}
      Format = 'single';
    case {'FLOAT64','H5T_IEEE_F64LE','H5T_IEEE_F64BE', ...
      'H5T_UNIX_D64BE','H5T_UNIX_D64LE','H5T_IEEE_F64BE','H5T_IEEE_F64LE'}
      Format = 'double';
    case {'H5T_STD_I8BE','H5T_STD_I8LE'}
      Format = 'int8';
    case {'H5T_STD_U8BE','H5T_STD_U8LE'}
      Format = 'uint8';
    case {'SHORT','H5T_STD_I16BE','H5T_STD_I16LE'}
      Format = 'int16';
    case {'USHORT','H5T_STD_U16BE','H5T_STD_U16LE'}
      Format = 'uint16';
    case {'LONG','H5T_STD_I32BE','H5T_STD_I32LE'}
      Format = 'int32';
    case {'ULONG','H5T_STD_U32BE','H5T_STD_U32LE'}
      Format = 'uint32';
    case {'H5T_STD_I64BE','H5T_STD_I64LE'}
      Format = 'int64';
    case {'H5T_STD_U64BE','H5T_STD_U64LE'}
      Format = 'uint64';
    end
    % - By default, the data storage class is identical to the definition class
    strStorageClass = Format;
    

    % - Parse class argument
    switch(lower(Format))
        case {'char'}
           nBytes = 2;
           strStorageClass = 'uint16';
           
        case {'int8', 'uint8'}
           nBytes = 1;
           
        case {'logical'}
           nBytes = 1;
           strStorageClass = 'uint8';
           
        case {'int16', 'uint16'}
           nBytes = 2;
           
        case {'int32', 'uint32', 'single','float32'}
           nBytes = 4;
           
        case {'int64', 'uint64', 'double','float64'}
           nBytes = 8;
           
        otherwise
           error('MappedTensor:InvalidClass', ...
           ['*** MappedTensor/ClassSize: Invalid class specifier: ' Format ]);
    end
    end
