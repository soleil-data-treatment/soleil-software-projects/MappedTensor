function out = h5_getdata(self, varargin)
% from https://fr.mathworks.com/matlabcentral/fileexchange/31703-hdf5-diskmap-class
      
  h5_open(self);
  
  % return all data
  if nargin == 1 || all(strcmp(varargin,':'))
    out = H5D.read(self.H5_dataset_id,'H5ML_DEFAULT','H5S_ALL','H5S_ALL','H5P_DEFAULT');
    return;
  end
  
  outsize = [];
  
  % vnOriginalSize = size(self);
  space = H5D.get_space(self.H5_dataset_id);
  [ans, dims] = H5S.get_simple_extent_dims(space);
  vnOriginalSize = fliplr(dims);
  dim = numel(vnOriginalSize);
  % fill singular dimensions if there's only 1 nonsingular dimension
  if dim > 1 && numel(varargin)==1 && sum(vnOriginalSize ~= 1) <= 1
    a = varargin;
    varargin = repmat({':'},1,dim);
    if any(vnOriginalSize ~= 1)
      varargin{vnOriginalSize ~= 1} = a{1};
    end
    outsize = size(a{1});
    if sum(outsize~=1)==1
      outsize = [];
    end
  end
  if dim ~= numel(varargin)
    if numel(varargin) == 1 && isnumeric(varargin{1})
      varg=cell(1,dim);
      [varg{:}] = ind2sub(vnOriginalSize, varargin{1});
      varargin = varg;
    else
      error([ mfilename ': target HDF5 dataset is ' num2str(dim) ' dimensions, but provided subscript data is ' num2str(numel(varargin)) ' dimensions.' ]);
    end
  end
  
  % divide selection into hyperslabs
  start = nan(1,dim);
  count = nan(1,dim);
  stride = nan(1,dim);
  hyperslab = true;
  for n = 1:dim
    sel = varargin{dim+1-n};  % traverse reversed instead of fliplr
    if isa(sel,'char') && sel == ':'
      start(n) = 1;
      count(n) = vnOriginalSize(dim+1-n);
      stride(n) = 1;
      idxc{n} = sel;
      selc{n} = sel;
    else
      %if isempty(sel)
      %  % if one dimension is empty the result is empty too
      %  out = [];
      %  return
      %end
      
      sel = sel(:);
      if isa(sel,'logical')
        sel = find(sel);
        idxc{n} = ':';
      elseif numel(sel) <= 1 || ( issorted(sel) && min(diff(sel))>0 )
        idxc{n} = ':';
      else
        [sel, ans, idxc{n}] = unique(sel);
      end
      
      selc{n} = sel;
      
      count(n) = numel(sel);

      dsel = diff(sel);
      if isempty(sel)
        start(n) = 1;
        stride(n) = 1;
      elseif numel(sel)==1
        start(n) = sel(1);
        stride(n) = 1;
      elseif all(dsel == dsel(1))
        start(n) = sel(1);
        stride(n) = dsel(1);
      else
        hyperslab = false;
      end
    end
  end

  % TODO: fix datatype 
  if any(count==0)
    out = zeros(fliplr(count));
    return
  end
  
  space=H5D.get_space(self.H5_dataset_id);
  if hyperslab
    %H5ML.hdf5lib2('H5Sselect_hyperslab',space_double,'H5S_SELECT_SET',start,stride,count,[]);
    H5S.select_hyperslab(space,'H5S_SELECT_SET',start-1,stride,count,[]);
  else
    s = find(isnan(start));
    selend = cellfun(@(x) x(end),selc(s));
    sel1 = cellfun(@(x) x(1),selc(s));
    if prod(count(s)) > .1 * prod(selend - sel1)
      % since hyperslab is way faster than select_elements, it is
      % faster to read a full hypeslab and do the selection in
      % matlab. The factor above should match the speed ratio.
      start(s) =  sel1;
      stride(s) = 1;
      count(s) = selend-sel1+1;
      for n=s
        idxc{n} = selc{n}(idxc{n}) - selc{n}(1) + 1;
      end
      H5S.select_hyperslab(space,'H5S_SELECT_SET',start-1,stride,count,[]);
    else
      sel = zeros(dim,prod(count));
      % ndgrid of selc{:}
      for n=1:dim,
        if ischar(selc{n})
          x = (1:count(n))';
        else
          x = selc{n}; % Extract and reshape as a vector.
        end
        s = count; s(n) = []; % Remove i-th dimension
        x = reshape(x(:,ones(1,prod(s))),[length(x) s]); % Expand x
        x = permute(x,[2:dim-n+1 1 dim-n+2:dim]); % Permute to i'th dimension
        sel(n,:) = x(:);
      end
      %warning('using H5S.select_elements, might be slow')
      H5S.select_elements(space,'H5S_SELECT_SET',sel-1);
    end
  end
  
  % reserve space for reading
  mem_space = H5S.create_simple(dim, count, []);

  % read the actual data
  out = H5D.read(self.H5_dataset_id,'H5ML_DEFAULT',mem_space,space,'H5P_DEFAULT');
  
  % shuffle output to match selection
  out=out(idxc{end:-1:1});
  
  if ~isempty(outsize)
    out = reshape(out,outsize);
  end

  H5S.close(space);
  H5S.close(mem_space);
  
end
