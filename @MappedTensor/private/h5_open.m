function file_id = h5_open( self )
    % h5_open Opens the HDF file property
    %   file_id = h5_open( mtVar, 'arg1', val1, ...)
    %
    % Options for the creation of the dataset:
    %       'size'        - size of the dataset. Necessary.
    %       'chunk_size'  - size of the chunks of the dataset.
    %       'compression' - 0-9  compression level.
    %       'type'        - type of the dataset to create.  This can be a string
    %                       such as 'double', in which case the datatype maps to
    %                       'H5T_NATIVE_DOUBLE', or it can be a derived HDF5 
    %                       datatype.
    %       'max_size'    - the maximum size of the dataset to create.
    %       'fill'        - the fill value.
      file_id = [];
      if ~isempty(self.H5_dataset) && ischar(self.H5_dataset) ...
      && (~isa(self.H5_dataset_id, 'H5ML.id') || self.H5_dataset_id.identifier < 0)
        if self.Writable
          mode = 'H5F_ACC_RDWR';
        else
          mode = 'H5F_ACC_RDONLY';
        end
        
        if ~exist(self.Filename, 'file')
          file_id = H5F.create(self.Filename, 'H5F_ACC_EXCL', 'H5P_DEFAULT', 'H5P_DEFAULT');
          mode    = 'H5F_ACC_RDWR';
          self.Writable = true;
        else
          file_id = H5F.open(self.Filename, mode, 'H5P_DEFAULT');
        end
        try
          self.H5_dataset_id = H5D.open(file_id,self.H5_dataset);
        catch ME
          if strcmp(ME.identifier, 'hdf5prop:exists')
            rethrow(ME);
          end
          % create dataset
          H5F.close(file_id);
          file_id = H5F.open(self.Filename, mode, 'H5P_DEFAULT');
          args={'size',       self.vnOriginalSize, ...
                'type',       self.Format};
          if ~isempty(self.H5_chunk_size) && isnumeric(self.H5_chunk_size)
            args{end+1} = 'chunk_size';
            args{end+1} = self.H5_chunk_size;
          end
          if ~isempty(self.bCompressed) && isnumeric(self.bCompressed)
            args{end+1} = 'compression';
            args{end+1} = self.bCompressed;
          end
          if ~isempty(self.H5_max_size) && isnumeric(self.H5_max_size)
            args{end+1} = 'max_size';
            args{end+1} = self.H5_max_size;
          end
          if ~isempty(self.H5_fill) && isnumeric(self.H5_fill)
            args{end+1} = 'fill';
            args{end+1} = self.H5_fill;
          end
          self.H5_dataset_id = h5datacreate(file_id, self.H5_dataset, args{:});
        end

        % get size of dataset
        % TODO: size is not valid when file is changed elsewhere while open
        space = H5D.get_space(self.H5_dataset_id);
        [ndims, dims] = H5S.get_simple_extent_dims(space);
        self.vnOriginalSize = fliplr(dims);
        self.hRealContent = file_id;
      end
    end
