function [tData] = h5_read_data_chunks(mtVar, mnFileChunkIndices, vnUniqueIndices, vnReverseSort, vnDataSize)
% H5_READ_DATA_CHUNKS - FUNCTION Read data without sorting or checking indices
% 'vnUniqueIndices' MUST be sorted and unique; 'vnReverseSort' must be the
% inverting indices from calling UNIQUE

    nNumChunks = size(mnFileChunkIndices, 1);

    % - Read data in chunks
    nDataPointer = 1;
    ind = [];
    for (nChunkIndex = 1:nNumChunks)
        % - Get chunk info
        nChunkSkip = mnFileChunkIndices(nChunkIndex, 2);
        nChunkSize = mnFileChunkIndices(nChunkIndex, 3);
        
        % - Seek file to beginning of chunk
        % fseek(hDataFile, (mnFileChunkIndices(nChunkIndex, 1)-1) * nClassSize + Offset, 'bof');
        start_index = mnFileChunkIndices(nChunkIndex, 1);
        
        % - Normal forward read
        % vUniqueData(nDataPointer:nDataPointer+nChunkSize-1) = fread(hDataFile, nChunkSize, [strStorageClass '=>' Format], (nChunkSkip-1) * nClassSize);
        end_index = start_index + (nChunkSize-1);
        ind       = [ ind start_index:end_index ];
        
        % - Shift to next data chunk
        nDataPointer = nDataPointer + nChunkSize;
    end
    vUniqueData = h5_getdata(mtVar, ind);

    % - Assign data back to original indexing order
    tData = reshape(vUniqueData(vnReverseSort), vnDataSize);
end
