function h5_setdata(self, argin, B)
% from https://fr.mathworks.com/matlabcentral/fileexchange/31703-hdf5-diskmap-class

  if ~self.Writable
    error([ self.Filename ': this hdf5 dataset is read only' ])
  end
  
  h5_open(self);
  
  % vnOriginalSize = size(self);
  space = H5D.get_space(self.H5_dataset_id);
  [ans, dims] = H5S.get_simple_extent_dims(space);
  self.vnOriginalSize = fliplr(dims);
  dim = numel(vnOriginalSize);
  % fill singular dimensions if there's only 1 nonsingular dimension
  if dim > 1 && numel(argin)==1 && sum(vnOriginalSize ~= 1) <= 1
    a = argin;
    argin = repmat({':'},1,dim);
    if any(vnOriginalSize ~= 1)
      argin{vnOriginalSize ~= 1} = a{1};
    end
  end
  if dim ~= numel(argin)
    if numel(argin) == 1 && isnumeric(argin{1})
      varg=cell(1,dim);
      [varg{:}] = ind2sub(vnOriginalSize, argin{1});
      argin = varg;
    else
      error([ mfilename ': target HDF5 dataset is ' num2str(dim) ' dimensions, but provided subscript data is ' num2str(numel(argin)) ' dimensions.' ]);
    end
  end
  
  colondim = strcmp(argin,':');
  
  % get input indexing dimensions
  indim = 1:dim;
  for n=1:dim
    if colondim(n)
      indim(n) = vnOriginalSize(n);
    elseif isa(argin{n},'logical')
      indim(n) = sum(argin{n}(:));
    else
      indim(n) = numel(argin{n});
    end
  end
  
  % check if squeezed dimensions match
  indim = max(cellfun(@numel,argin), colondim.*vnOriginalSize);
  szB = size(B);
  assert(numel(B)==1 || all(indim(indim~=1)==szB(szB~=1)),'Subscripted assignment dimension mismatch.')
  
  if any(indim==0)
    % nothing to be done
    return;
  end
  
  % unsqueeze input
  if numel(B)~=1 && 0
    B = reshape(B,indim);
  end
  
  % write all data
  if all(colondim)
    if numel(B) == 1
      B = repmat(B,vnOriginalSize);
      % H5D.fill ?
    end
    
    H5D.write(self.H5_dataset_id,'H5ML_DEFAULT','H5S_ALL','H5S_ALL','H5P_DEFAULT',B);
    return;
  end
  
  % divide selection into hyperslabs
  start = zeros(1,dim);
  count = zeros(1,dim);
  stride = zeros(1,dim);
  hyperslab = true;
  for n = 1:dim
    sel = argin{dim+1-n};  % traverse reversed instead of fliplr
    if isa(sel,'char') && sel == ':'
      start(n) = 1;
      count(n) = vnOriginalSize(dim+1-n);
      stride(n) = 1;
      idxc{n} = sel;
      selc{n} = sel;
    else
      sel = sel(:);
      if isa(sel,'logical')
        sel = find(sel);
        idxc{n} = ':';
      elseif numel(sel) == 1 || ( issorted(sel) && min(diff(sel))>0 )
        idxc{n} = ':';
      else
        % this line is different from get_data
        [sel, idxc{n}] = unique(sel);
      end
      
      selc{n} = sel;
      
      count(n) = numel(sel);
      if hyperslab
        dsel = diff(sel);
        if numel(sel)==1
          start(n) = sel(1);
          stride(n) = 1;
        elseif all(dsel == dsel(1))
          start(n) = sel(1);
          stride(n) = dsel(1);
        else
          hyperslab = false;
        end
      end
    end
  end
  
  space=H5D.get_space(self.H5_dataset_id);
  if hyperslab
    %H5ML.HDF5lib2('H5Sselect_hyperslab',space_double,'H5S_SELECT_SET',start,stride,count,[]);
    H5S.select_hyperslab(space,'H5S_SELECT_SET',start-1,stride,count,[]);
  else
    sel = zeros(dim,prod(count));
    % ndgrid of selc{:}
    for n=1:dim,
      if ischar(selc{n})
        x = (1:count(n))';
      else
        x = selc{n}; % Extract and reshape as a vector.
      end
      s = count; s(n) = []; % Remove i-th dimension
      x = reshape(x(:,ones(1,prod(s))),[length(x) s]); % Expand x
      x = permute(x,[2:dim-n+1 1 dim-n+2:dim]); % Permute to i'th dimension
      sel(n,:) = x(:);
    end
    H5S.select_elements(space,'H5S_SELECT_SET',sel-1);
  end
  
  % reserve space for reading
  mem_space = H5S.create_simple(dim, count, []);

  % prepare input
  if prod(count) > 1
    if numel(B) == 1
      B = repmat(B,count);
    elseif isvector(B)
      % nop
    else
      % shuffle input to match selection
      B = B(idxc{end:-1:1});
    end
  end
  
  % write the actual data
  H5D.write(self.H5_dataset_id,'H5ML_DEFAULT',mem_space,space,'H5P_DEFAULT',B);
  
  H5S.close(space);
  H5S.close(mem_space);
end
