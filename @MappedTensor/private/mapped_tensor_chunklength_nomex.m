function [nChunkLength] = mapped_tensor_chunklength_nomex(vfDiffs, nIndex)
% MAPPED_TENSOR_CHUNKLENGTH_NOMEX Calcluate the length of a chunk

% this function is only used from within SplitFileChunks

    nChunkLength = find(vfDiffs(nIndex+1:end) ~= vfDiffs(nIndex), 1, 'first');
end
