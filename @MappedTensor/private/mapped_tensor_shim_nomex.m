function [varargout] = mapped_tensor_shim_nomex(strCommand, varargin)
    switch (strCommand)
        case 'open'
        % syntax: [file_handle, MachineFormat] = hShimFunc('open', ~Writable, Filename);
           if (nargin == 2)
              [varargout{1}] = fopen(varargin{1}, 'r+');
              [nul, nul, varargout{2}, nul] = fopen(varargout{1}); %#ok<ASGLU,NASGU>
           else
              varargout{1} = fopen(varargin{1}, 'r+', varargin{2});
           end
           
        case 'close'
        % syntax hShimFunc('close', hFileHandle);
           fclose(varargin{1});
           
        case 'read_chunks'
        % syntax: [tData] = hShimFunc('read_chunks', ...
        %   hDataFile, mnFileChunkIndices, vnUniqueIndices, vnReverseSort, vnDataSize, Format, Offset)
           varargout{1} = mt_read_data_chunks(varargin{1:7});
           
        case 'write_chunks'
        % syntax: hShimFunc('write_chunks', ...
        %   hDataFile, mnFileChunkIndices, vnUniqueDataIndices, vnDataSize, Format, Offset, tData);
           mt_write_data_chunks(varargin{1:7});
           
        case 'read_all'
        % syntax: [tData] = hShimFunc('read_all', ...
        %   hDataFile, vnTensorSize, Format, Offset, ~) 
        %   last arg = bBigEndian is not used
           varargout{1} = mt_read_all(varargin{1:5});
           
        case 'write_all'
        % syntax:hShimFunc('write_all', ...
        %   hDataFile, vnTensorSize, Format, Offset, tData, ~)
        %   last arg = bBigEndian is not used
           varargout{1} = mt_write_all(varargin{1:6});
    end
end
