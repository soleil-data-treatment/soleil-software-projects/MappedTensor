function file_id = mat_open( self )
    % MAT_OPEN Opens the MAT file property
    %   file_id = mat_open( mtVar, 'arg1', val1, ...)

    if ~isempty(self.MAT_dataset) && ischar(self.MAT_dataset) ...
    && ~isa(self.hRealContent, 'matlab.io.MatFile')

      if ~exist(self.Filename, 'file')
        self.Writable = true;
      end
      self.hRealContent = matfile(self.Filename, 'Writable', self.Writable);

    elseif self.Writable && isa(self.hRealContent, 'matlab.io.MatFile')
      self.hRealContent.Properties.Writable = true;
    end
    
    file_id = self.hRealContent;
    self.hShimFunc    = [];
