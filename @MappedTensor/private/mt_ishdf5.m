function [tf, filename, dataset] = mt_ishdf5(filename)
% MT_ISHDF5 Check if filename/object is HDF5/MAT
%   Returns 1 for HDF5, 2 for MAT, 0 otherwise.

  dataset = []; tf = false;
  if isa(filename, 'MappedTensor')
    if ~isempty(filename.H5_dataset) && ischar(filename.H5_dataset)
    % existing MappedTensor is HDF5 ?
      tf       = 1;
      self     = filename;
      filename = self.Filename;
      dataset  = self.H5_dataset;
    elseif ~isempty(filename.MAT_dataset) && ischar(filename.MAT_dataset)
    % existing MappedTensor is MAT ?
      tf       = 2;
      self     = filename;
      filename = self.Filename;
      dataset  = self.MAT_dataset;
    end
  elseif ~isa(filename, 'MappedTensor') && ischar(filename)
  % HDF5/MAT "filename.[h5|hdf5|hdf|mat]:dataset" or "filename.[h5|hdf5|hdf|mat]/dataset"
    [p,f,e] = fileparts(filename);
    ext     = strtok(e, ':/');
    if any(strcmp(ext(2:end),{'hdf5','h5','hdf','nx','nxs','he5','mat'}))
      ext = ext(2:end);
      for tok={ ['.' ext{1} '::'], [ '.' ext{1} '//' ], ['.' ext{1} ':'], ['.' ext{1} '/']}
        loc = strfind(lower(filename), tok{1});
        if ~isempty(loc)
          dataset = filename(  loc+numel(tok{1}):end); % after '.ext:' or '.ext/'
          filename= filename(1:loc+numel(ext{1}));
          if strcmp(ext, 'mat'), tf = 2; else tf = 1; end
          return
        end
      end
    end
  end
