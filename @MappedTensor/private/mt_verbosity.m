function v=mt_verbosity(v)
% MT_VERBOSITY get or set verbosity level
  persistent verbosity
  if isempty(verbosity), verbosity=0; end
  if nargin == 0
    v = verbosity;
  elseif isnumeric(v) && isscalar(v)
    verbosity = v;
  end
end
