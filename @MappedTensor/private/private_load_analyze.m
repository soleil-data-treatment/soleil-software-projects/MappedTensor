function [Descr, args, header] = private_load_analyze(filename)
% PRIVATE_LOAD_ANALYZE Read an Analyze 7.5 Mayo Clinic file
%  data = private_load_analyze(filename)
%
% Format definition <https://web.archive.org/web/20070927191351/http://www.mayo.edu/bir/PDF/ANALYZE75.pdf>
%
% Input:  filename: Analyze HDR/IMG file (string)
% output: structure

header = readanalyze_header(filename);
Descr  = header.descr;

args = { ...
  'Offset',         0, ...
  'Format',         header.dtype, ...
  'MachineFormat',  header.endian, ...
  'Dimension',      header.dimension' };
  

% ------------------------------------------------------------------------------
function header = readanalyze_header(filename)


% Get rid of possible file extensions
[p,f,e]  = fileparts(filename);
filename = fullfile(p,f);
header.endian = '';
% Open Header in big/little endian (modern systems are little endian)
for endian={'ieee-le','ieee-be'}
  [fid, mess] = fopen([filename '.hdr'],'r',endian{1});
  if fid < 0
    error([ mfilename ': Error opening header file ' filename ': ' mess ]);
  end
  header.header_size=fread(fid,2,'int16');
  if any(header.header_size == 348)
    header.endian=endian{1};
    break % keep file opened
  else
    fclose(fid);
  end
end
if isempty(header.endian)
  error([ mfilename ': ' filename ': probably not an Analyze 7.5 file format.' ]);
end

% reading header from file -----------------------------------------------------
fread(fid,36,'uchar');           % dummy read header information
header.dims=fread(fid,1,'ushort');        % dimension (3 or 4)
header.dimension=fread(fid,4,'ushort');   % dimension, number of pixels
if (header.dims == 3) || (header.dimension(4) == 1)
  header.dimension=header.dimension(1:3);
end;  
fread(fid,4,'ushort');          
fread(fid,6,'ushort');          
header.Datatype=fread(fid,1,'ushort');    % datatype  
header.BitsPerVoxel=fread(fid,1,'ushort');% Bits per voxel
fread(fid,1,'ushort');
fread(fid,2,'ushort');        
header.vixel_size=fread(fid,3,'float32'); % size of pixels
fread(fid,4,'float32');
header.offset=fread(fid,1,'float32');     % offset for pixels (funused8), SPM extension
header.scale=fread(fid,1,'float32');      % scaling for pixels (funused9), SPM extension

fread(fid,24,'char');

header.limits=fread(fid,2,'int');         % Limits for number in given analyze format

descr_input=fread(fid,80,'char');         % Description field in header file
header.descr=char(descr_input)';
header.descr=strtrim(header.descr);
Descr = [ header.descr ' Analyze Mayo Clinic' ];

fread(fid,24,'char');
header.orient=fread(fid,1,'char');        % Orientation, not used
% Currently orient is not used.
%  orient:     slice orientation for this dataset. 
%       0      transverse unflipped 
%       1      coronal unflipped 
%       2      sagittal unflipped 
%       3      transverse flipped 
%       4      coronal flipped 
%       5      sagittal flipped 

header.origin=fread(fid,3,'int16');       % Origin, SPM extension to analyze format

fread(fid,89,'char');              % Not used
fclose(fid);

switch header.Datatype
  case 2
    header.dtype = 'uint8';
  case 4
    header.dtype = 'int16';
  case 8
    header.dtype = 'int32';
  case 16
    header.dtype = 'single';
  case 64
    header.dtype = 'double';
  case 132
    header.dtype = 'int16';
  otherwise
    error([ mfilename ': Unsupported datatype ' num2str(header.Datatype) ]);
end



