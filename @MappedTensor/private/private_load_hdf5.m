function [Descr, args, header] = private_load_hdf5(filename, dataset)
% PRIVATE_LOAD_HDF5 Read an HDF5 file
%  data = private_load_hdf5(filename, dataset)
%
% Reference:
%   https://en.wikipedia.org/wiki/Hierarchical_Data_Format
%
% Input:  filename: HDF5 file (string)
%         dataset: data set path (string)
% output: structure

  Descr=''; args={}; header=[];

  if nargin ==1 || ~ischar(dataset)
    dataset='/*';
  end
  info1 = [];
  % extract any query/fragment
  [dataset, query, fragment] = get_query(filename, dataset);
  if isempty(query) && isempty(fragment)
    try
      info1 = h5info(filename,dataset);
    end
  end

  if isempty(info1)
    info1 = h5info(filename);
    % recursive call
    info1 = getGroup(info1, dataset, query, fragment);
  end

  if ~iscell(info1)
    info1 = { info1 };
  end

  % assemble returned info/datasets
  header = cell(1,numel(info1)); Descr = header; args = header;
  for index=1:numel(info1)
    H = info1{index};
    
    D = H.Name; % Dataset
    
    A = {
    'Offset',         0, ...
    'Format',         H.Datatype.Type, ...
    'Dimension',      H.Dataspace.Size };
    
    if isfield(H,'Filename')
      A = { 'Filename', H.Filename, A{:} }; % must be first
    end

    % store
    Descr{index} = D; header{index} = H; args{index} = A;
  end
end
  
% ------------------------------------------------------------------------------
% code from ifit/io/read_hdf5

function data = getGroup(data_info, dataset, query, fragment)
% getGroup recursively traverse the HDF tree

  if nargin < 2, dataset=[];  end
  if nargin < 3, query=[];    end
  if nargin < 4, fragment=[]; end
  if isempty(dataset) && ~isempty(fragment)
    dataset = fragment;
    fragment= [];
  end

  if ischar(data_info) && any(data_info == ':')
    [tf, filename, datapath] = mt_ishdf5(data_info);
    if tf
      % dataset is a link to a file:path
      data_info = filename;
      dataset   = datapath;
    end
  end

  if (nargin == 1 || (ischar(data_info) && ~isempty(dir(data_info))))
    data_info = h5info(data_info);
  end

  data       = {};
  root = data_info.Name;
  if ~strcmp(root, '/'), root = [ root  '/' ]; end
  
  % HDF5_dataset must match:
  %   Name        # dataset  when given, as string or regexp
  %   Name        # fragment when given, as string or regexp (same as dataset name check)
  %   attributes  # query    when given, as cellstr of attr_name[=attr_value]

  % Get group datasets
  nvars   = length(data_info.Datasets);
  for i = 1: nvars
    dataID = [root data_info.Datasets(i).Name];
    if isempty(dataID), continue; end
    keep   = false;
    if any(strcmp(data_info.Datasets(i).Datatype.Class, {'H5T_FLOAT','H5T_INTEGER','H5T_ARRAY'}))
      if      isempty(dataset)  || check_name(dataID, dataset)
        keep = true; % Name of the HDF5_dataset matches 'dataset'
      elseif ~isempty(fragment) && check_name(dataID, fragment)
        keep = true; % Name of the HDF5_dataset matches 'fragment'
      end
      % was selected from Name ? check attributes
      if keep && ~isempty(query)
        % when query is given, it MUST match all queries
        if ~all(check_query(query, data_info.Datasets(i)))
          keep=false;
        end
      end
      if keep
        this = data_info.Datasets(i);
        this.Name = dataID;
        data{end+1} = this;
      end
    end
  end
  
  % Get Links
  if isfield(data_info, 'Links') && ~isempty(data_info.Links)
    nlinks  = length(data_info.Links);
    for i = 1: nlinks
      dataID = data_info.Links(i).Name;
      if isfield(data_info.Links(i),'Value')
        val  = data_info.Links(i).Value; % target dataset
      elseif isfield(data_info.Links(i),'Target')
        val  = data_info.Links(i).Target;% target dataset
      else continue
      end
      val = cellstr(val);
      if numel(val) == 2
        % "external_file:path" check if the target file exists
        if ~isempty(dir(val{1}))
          val = { sprintf('%s:%s', val{1},val{2}) };
        else continue;  % target does not exist
        end
      end
      val = char(val);
      if strcmp(val, dataset), continue; end
      % disp([ mfilename ': ' root ':' dataset ' Link: ' dataID '=' char(val) ]);
      
      if ~isempty(dataID) && (isempty(dataset) ...
      || check_name(dataID, dataset) || check_name(val, dataset))
        this = getGroup(data_info, val, query, fragment);
        if ~isempty(this)
          this.Name = dataID
          data = [ data this ];
        end
      end
    end
  end

  % Get each subgroup
  ngroups = length(data_info.Groups);
  for i = 1 : ngroups
    group = getGroup(data_info.Groups(i), dataset, query, fragment);
    if ~isempty(group)
      data = [ data group ]; 
    end
  end
end % getGroup

% ------------------------------------------------------------------------------
function [dataset, attr, fragment] = get_query(filename, dataset)
% GET_QUERY get and split query string around ? and & from URL filename:dataset
%   The attributes for an HDF5 dataset can be given with syntax
%     [Name]=[Value]
%   where [Name] is an attribute name, and [Value] is the expected value.
%
%   In addition the following attributes are always defined:
%     shape = size  holds the dataset size as [n1 n2 ...]
%     ndim  = ndims holds the dataset dimensionality (1,2,...)
%     len   = numel holds the dataset number of elements ('size' in NumPy)
%     dtype = class holds the dataset storage class (double, single, char, float, integer...)
%
%   and for each attribute, you may apply methods with syntax:
%     attr.method(arg)=val is equilanent to method(attr, arg)==val
%   for instance:
%     attr.eq(5) is the same as attr=5 and eq(attr, 5)
%     attr.gt(5) is the same as attr>5 and gt(attr, 5)
%     attr.numel>5 is the same as numel(attr)>5
%     attr.size>5  is the same as all(size(attr)>5)
%     attr.size>[5 1]  is the same as all(size(attr)>[5 1])
%     attr.ischar  is the same as ischar(attr)
%
%   such as:
%     /?filename=153265_153282_LAMP.hdf   check for attribute value
%     /entry1/data1?class                 just check if attribute exists
%     /entry1/data1?class=NXdata          check for group attribute value
%     /entry1/data1/DATA?signal=1         check for dataset attribute value
%     

  res     = urlParse([ filename ':' dataset ]);
  attr    = res.query; % may contain multiple entries
  dataset = res.path;
  if ~isempty(attr)
    attr    = textscan(attr, '%s','Delimiter','?&;'); 
    attr    = attr{1};
    attr    = attr(~cellfun(@isempty, attr));
  end
  fragment= res.fragment;
  if numel(fragment) > 1
    fragment = fragment(2:end);
  end
end % GET_QUERY

% ------------------------------------------------------------------------------
function tf = check_name(dataID, dataset)
% CHECK_NAME check that the required 'dataset' matches the current dataID (Name)
%   TF=CHECK_NAME(dataID, name) returns true when dataID contains/matches name
%   search is case sensitive.
%
%   [TF]=CHECK_NAME({dataID1, dataID2, ...}, name) returns logical array where dataID contains/matches name
  if iscellstr(dataID)
    tf = zeros(size(dataID));
    for index=1:numel(dataID)
      tf(index) = check_name(dataID{index}, dataset);
    end
    return
  else
    tf = strcmp(dataID, dataset) | ~isempty(regexp(dataID, dataset));
  end
end
% ------------------------------------------------------------------------------

function tf = check_query(queries, DataSet)
% CHECK_QUERY check that ALL queries are satisfied
  Attributes = DataSet.Attributes;
  if isempty(Attributes)
    Attributes = struct('Name','','Value','','Datatype','','Dataspace','');
  end
  a0 = Attributes(1);
  % add standard Attributes (matlab and python equivalent)
  a0.Name='Size';  a0.Value=DataSet.Dataspace.Size; Attributes(end+1) = a0;
  a0.Name='size';  Attributes(end+1) = a0;
  a0.Name='shape'; Attributes(end+1) = a0;
  nd = numel(DataSet.Dataspace.Size); if numel(find(nd>1)) == 1, nd=1; end
  a0.Name='ndim';  a0.Value=nd; Attributes(end+1) = a0;
  a0.Name='ndims'; a0.Value=nd; Attributes(end+1) = a0;
  a0.Name='len';   a0.Value=prod(DataSet.Dataspace.Size); Attributes(end+1) = a0;
  a0.Name='numel'; Attributes(end+1) = a0;
  [nBytes, strStorageClass, Format] = ClassSize(DataSet.Datatype.Type);
  a0.Name='type'; a0.Value=Format; Attributes(end+1) = a0;
  a0.Name='class'; a0.Value=Format; Attributes(end+1) = a0;
  a0.Name='dtype'; a0.Value=DataSet.Datatype.Type; Attributes(end+1) = a0;
  
  % parse Attributes and check queries
  tf = zeros(1,numel(queries));
  for q=1:numel(queries)
    % extract name.method of query
    query = queries{q};
    [name,check]=strtok(query, '<=>~!');
    
    % extract method to apply
    [name,meth]=strtok(name, '.');
    if ~isempty(meth), meth = meth(2:end); end
    % extract value to check
    ops_char = {'<=','>=','~=','!=','<','=','>','=='};
    ops_fun  = {@le, @ge, @ne, @ne, @lt,@eq,@gt,@eq};
    op = '';
    for index=1:numel(ops_char)
      if strncmp(check, ops_char{index}, numel(ops_char{index}))
        op    = ops_fun{index}; 
        check = check(numel(ops_char{index})+1:end);
        break;
      end
    end
    
    % now we have [name].[meth][op][check]
    
    % find the Attributes that matches 'name'
    j = find(check_name({ Attributes.Name }, name),1);
    tf(q) = ~isempty(j);
    
    % name matches ? check for other tests
    if tf(q) && (~isempty(check) || ~isempty(meth)) 
      attrID = Attributes(j).Name;
      val    = Attributes(j).Value;
      if strcmp(class(val), 'hdf5.h5string'), val = char(val.Data); end
      if iscellstr(val) && length(val) == 1,  val = char(val); end
      if ~ischar(val), val=double(val); end
      
      % apply method on attribute value when exists
      if ~isempty(meth)
        % apply method, with optional arguments
        i1  = find(meth == '(', 1);
        i2  = find(meth == ')', 1, 'last');
        if ~isempty(i1) && ~isempty(i2)
          m1 = meth(1:i1);
          m2 = meth((i1+1):(i2-1));
          if ~isempty(m2), m2 = [ ',' m2 ]; end
          m3 = meth(i2:end);
          meth = [ m1 'val' m2 m3];
        elseif isempty(i1) && isempty(i2)
          meth = [ meth '(val)' ];
        end
        try
          eval([ 'val=' meth ';' ]);
        catch ME
          warning([ mfilename ': invalid expression from attribute ' attrID ' method ' meth ]);
          disp(getReport(ME))
        end
      end
      
      % test attribute value against 'check'
      if ~isempty(op) && ~isempty(check)
        try
          if isnumeric(val)
            tf(q) = all(feval(op, val, str2num(check)));
          elseif ischar(val)
            tf(q) = check_name(val, check);
          end
        catch ME
          warning([ mfilename ': invalid value check ' attrID ' ' char(op) ' ' check ]);
          disp(getReport(ME))
        end
      else
        tf(q) = all(val(:));
      end
    end
  end
end
