function [Descr, args, header] = private_load_mat(filename, dataset)
% PRIVATE_LOAD_MAT Read an MAT file
%  data = private_load_mat(filename, dataset)
%
% Input:  filename: MAT 7.3 file (string)
%         dataset: data set path (string)
% output: structure

  Descr=''; args={}; header=[];

  if nargin ==1 || ~ischar(dataset)
    dataset='.*';
  end

  % extract any query/fragment
  [dataset, query, fragment] = get_query(filename, dataset);
  
  info1 = whos(matfile(filename));
  
  % search for dataset and query
  info1 = getGroup(info1, dataset, query, fragment);

  % assemble returned info/datasets
  header = cell(1,numel(info1)); Descr = header; args = header;
  for index=1:numel(info1)
    H = info1(index);
    
    D = H.name; % Variable name
    
    A = {
    'Offset',         0, ...
    'Format',         H.class, ...
    'Dimension',      H.size };

    % store
    Descr{index} = D; header{index} = H; args{index} = A;
  end
end
  
% ------------------------------------------------------------------------------

function data = getGroup(data_info, dataset, query, fragment)
% getGroup test for all available variables in the MAT

  if nargin < 2, dataset=[];  end
  if nargin < 3, query=[];    end
  if nargin < 4, fragment=[]; end
  if isempty(dataset) && ~isempty(fragment)
    dataset = fragment;
    fragment= [];
  end

  % MAT must match:
  %   name        # dataset  when given, as string or regexp
  %   name        # fragment when given, as string or regexp (same as dataset name check)
  %   attributes  # query    when given, as cellstr of attr_name[=attr_value]

  % Get group datasets
  tokeep = [];
  for i = 1:length(data_info)
    dataID = data_info(i).name;
    keep   = false;
    % must be numeric
    if any(strcmp(data_info(i).class, {'double','single','int8','int16', ...
      'int32','int64','int8','int16','int32','int64'}))
      if      isempty(dataset)  || check_name(dataID, dataset)
        keep = true; % Name of the MAT_dataset matches 'dataset'
      elseif ~isempty(fragment) && check_name(dataID, fragment)
        keep = true; % Name of the MAT_dataset matches 'fragment'
      end
      % was selected from Name ? check attributes
      if keep && ~isempty(query)
        % when query is given, it MUST match all queries
        if ~all(check_query(query, data_info(i)))
          keep=false;
        end
      end
      if keep
        tokeep(end+1) = i;
      end
    end
  end
  
  data = data_info(tokeep);

end % getGroup

% ------------------------------------------------------------------------------
function [dataset, attr, fragment] = get_query(filename, dataset)
% GET_QUERY get and split query string around ? and & from URL filename:dataset
%   The attributes for an HDF5 dataset can be given with syntax
%     [Name]=[Value]
%   where [Name] is an attribute name, and [Value] is the expected value.
%
%   In addition the following attributes are always defined:
%     shape = size  holds the dataset size as [n1 n2 ...]
%     ndim  = ndims holds the dataset dimensionality (1,2,...)
%     len   = numel holds the dataset number of elements ('size' in NumPy)
%     dtype = class holds the dataset storage class (double, single, char, float, integer...)
%
%   and for each attribute, you may apply methods with syntax:
%     attr.method(arg)=val is equilanent to method(attr, arg)==val
%   for instance:
%     attr.eq(5) is the same as attr=5 and eq(attr, 5)
%     attr.gt(5) is the same as attr>5 and gt(attr, 5)
%     attr.numel>5 is the same as numel(attr)>5
%     attr.size>5  is the same as all(size(attr)>5)
%     attr.size>[5 1]  is the same as all(size(attr)>[5 1])
%     attr.ischar  is the same as ischar(attr)
%
%   such as:
%     /?filename=153265_153282_LAMP.hdf   check for attribute value
%     /entry1/data1?class                 just check if attribute exists
%     /entry1/data1?class=NXdata          check for group attribute value
%     /entry1/data1/DATA?signal=1         check for dataset attribute value
%     

  res     = urlParse([ filename ':' dataset ]);
  attr    = res.query; % may contain multiple entries
  dataset = res.path;
  if ~isempty(attr)
    attr    = textscan(attr, '%s','Delimiter','?&;'); 
    attr    = attr{1};
    attr    = attr(~cellfun(@isempty, attr));
  end
  fragment= res.fragment;
  if numel(fragment) > 1
    fragment = fragment(2:end);
  end
end % GET_QUERY

% ------------------------------------------------------------------------------
function tf = check_name(dataID, dataset)
% CHECK_NAME check that the required 'dataset' matches the current dataID (Name)
%   TF=CHECK_NAME(dataID, name) returns true when dataID contains/matches name
%   search is case sensitive.
%
%   [TF]=CHECK_NAME({dataID1, dataID2, ...}, name) returns logical array where dataID contains/matches name
  if iscellstr(dataID)
    tf = zeros(size(dataID));
    for index=1:numel(dataID)
      tf(index) = check_name(dataID{index}, dataset);
    end
    return
  else
    tf = strcmp(dataID, dataset) | ~isempty(regexp(dataID, dataset));
  end
end
% ------------------------------------------------------------------------------

function tf = check_query(queries, Attributes)
% CHECK_QUERY check that ALL queries are satisfied

  % add standard Attributes (matlab and python equivalent)
  Attributes.Size   = Attributes.size;
  Attributes.shape  = Attributes.size;
  nd = numel(Attributes.size); if numel(find(nd>1)) == 1, nd=1; end
  Attributes.ndim   = nd; 
  Attributes.ndims  = nd; 
  Attributes.len    = prod(Attributes.size); 
  Attributes.numel  = Attributes.len;
  Attributes.type   = Attributes.class;
  Attributes.dtype  = Attributes.class;
  
  % parse Attributes and check queries
  tf = zeros(1,numel(queries));
  for q=1:numel(queries)
    % extract name.method of query
    query = queries{q};
    [name,check]=strtok(query, '<=>~!');
    
    % extract method to apply
    [name,meth]=strtok(name, '.');
    if ~isempty(meth), meth = meth(2:end); end
    % extract value to check
    ops_char = {'<=','>=','~=','!=','<','=','>','=='};
    ops_fun  = {@le, @ge, @ne, @ne, @lt,@eq,@gt,@eq};
    op = '';
    for index=1:numel(ops_char)
      if strncmp(check, ops_char{index}, numel(ops_char{index}))
        op    = ops_fun{index}; 
        check = check(numel(ops_char{index})+1:end);
        break;
      end
    end
    
    % now we have [name].[meth][op][check]
    
    % find the Attributes that matches 'name'
    tf(q) = isfield(Attributes, name);
    
    % name matches ? check for other tests
    if tf(q) && (~isempty(check) || ~isempty(meth)) 
      val    = Attributes.(name);
      
      % apply method on attribute value when exists
      if ~isempty(meth)
        % apply method, with optional arguments
        i1  = find(meth == '(', 1);
        i2  = find(meth == ')', 1, 'last');
        if ~isempty(i1) && ~isempty(i2)
          m1 = meth(1:i1);
          m2 = meth((i1+1):(i2-1));
          if ~isempty(m2), m2 = [ ',' m2 ]; end
          m3 = meth(i2:end);
          meth = [ m1 'val' m2 m3];
        elseif isempty(i1) && isempty(i2)
          meth = [ meth '(val)' ];
        end
        try
          eval([ 'val=' meth ';' ]);
        catch ME
          warning([ mfilename ': invalid expression from attribute ' name ' method ' meth ]);
          disp(getReport(ME))
        end
      end
      
      % test attribute value against 'check'
      if ~isempty(op) && ~isempty(check)
        try
          if isnumeric(val)
            tf(q) = all(feval(op, val, str2num(check)));
          elseif ischar(val)
            tf(q) = check_name(val, check);
          end
        catch ME
          warning([ mfilename ': invalid value check ' name ' ' char(op) ' ' check ]);
          disp(getReport(ME))
        end
      else
        tf(q) = all(val(:));
      end
    end
  end
end
