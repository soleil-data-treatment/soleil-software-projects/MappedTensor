function [Descr, args, header] = private_load_nii(filename)
% PRIVATE_LOAD_NII Read a NifTi Neuroimaging Informatics Technology Initiative volume
%  data = private_load_nii(filename)
%
% Format definition and consortium <http://nifti.nimh.nih.gov/>
%
% NifTi example data NII files at <https://nifti.nimh.nih.gov/nifti-1/data>
%
% references:
%   <http://www.mathworks.com/matlabcentral/fileexchange/29344-read-medical-data-3d>
%   by Dirk-Jan Kroon 10 Nov 2010 (Updated 23 Feb 2011)
%
% Input:  filename: NifTi file (string)
% output: structure

Descr=''; args = [];

header = nii_read_header(filename);

% compute offset, Seek volume data start
datasize=prod(header.Dimensions)*(header.BitVoxel/8);
fsize=header.Filesize;
header.Offset = fsize-datasize;

% checks for datatype: float -> single
if strcmpi(header.DataTypeStr,'float')
  header.DataTypeStr = 'single';
end

if header.headerbswap
  MachineFormat = 'ieee-be';
else
  MachineFormat = 'ieee-le';
end

Descr= [ header.Descrip ' NifTI-1 MRI volume data' ];
args = {
  'Offset',         header.Offset, ...
  'Format',         lower(header.DataTypeStr), ...
  'MachineFormat',  MachineFormat, ...
  'Dimension' ,     header.Dimensions };

% ------------------------------------------------------------------------------
%Copyright (c) 2010, Dirk-Jan Kroon
%All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are
%met:
%
%* Redistributions of source code must retain the above copyright
%  notice, this list of conditions and the following disclaimer.
%* Redistributions in binary form must reproduce the above copyright
%  notice, this list of conditions and the following disclaimer in
%  the documentation and/or other materials provided with the distribution
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%POSSIBILITY OF SUCH DAMAGE.

function [info] = nii_read_header(filename)
% function for reading header of NifTi ( .nii ) volume file
%
% info = nii_read_header(filename);
%
% examples:
% 1,  info=nii_read_header()
% 2,  info=nii_read_header('volume.nii');

if(exist('filename','var')==0)
     [filename, pathname] = uigetfile('*.nii', 'Read nii-file');
     filename = [pathname filename];
end

bswap=false;
test=true;
while(test)
    if(bswap)
        fid=fopen(filename,'rb','b');
    else
        fid=fopen(filename,'rb','l');
    end    
    if(fid<0)
         fprintf('could not open file %s\n',filename);
         return
    end

    %get the file size
    fseek(fid,0,'eof');
    info.Filesize = ftell(fid); 
    fseek(fid,0,'bof');
    info.Filename=filename;
    info.SizeofHdr=fread(fid,1,'int');
    info.DataType=fread(fid, 10, 'uint8=>char')';
    info.DbName=fread(fid, 18, 'uint8=>char')';
    info.Extents=fread(fid,1,'int');
    info.SessionError=fread(fid,1,'uint16');
    info.Regular=fread(fid, 1, 'uint8=>char')';
    info.DimInfo=fread(fid, 1, 'uint8=>char')';
    swaptemp=fread(fid, 1, 'uint16')';
    info.Dimensions=fread(fid,7,'uint16')'; % dim = [ number of dimensions x,y,z,t,c1,c2,c3];
	
    if(swaptemp(1)<1||swaptemp(1)>7), bswap=true; fclose(fid); else test=false; end
end

if info.SizeofHdr > info.Filesize || info.Extents > info.Filesize
  fprintf([ mfilename ': wrong file hdr extent %i %i > %i\n' ], info.SizeofHdr, info.Extents, info.Filesize);
end
    
info.headerbswap=bswap;
info.IntentP1=fread(fid,1,'float');
info.IntentP2=fread(fid,1,'float');
info.IntentP3=fread(fid,1,'float');
info.IntentCode=fread(fid,1,'uint16');
info.DataType=fread(fid,1,'uint16');
datatypestr{1}={0,'UNKNOWN',  0}; % what it says, dude           
datatypestr{2}={1,'BINARY',   1}; % binary (1 bit/voxel)         
datatypestr{3}={2,'UINT8'  ,  8};% unsigned char (8 bits/voxel) 
datatypestr{4}={4,'INT16'   , 16}; % signed short (16 bits/voxel) 
datatypestr{5}={8,'INT32'  ,  32}; % signed int (32 bits/voxel)   
datatypestr{6}={16,'FLOAT' ,  32}; % float (32 bits/voxel)        
datatypestr{7}={32,'COMPLEX', 64}; % complex (64 bits/voxel)      
datatypestr{8}={64,'DOUBLE',  64}; % double (64 bits/voxel)       
datatypestr{9}={128,'RGB'  ,  24}; % RGB triple (24 bits/voxel)   
datatypestr{10}={255,'ALL'  ,  0}; % not very useful (?)          
datatypestr{11}={256,'INT8' ,  8}; % signed char (8 bits)         
datatypestr{12}={512,'UINT16', 16}; % unsigned short (16 bits)     
datatypestr{13}={768,'UINT32', 32}; % unsigned int (32 bits)       
datatypestr{14}={1024,'INT64', 64}; % long long (64 bits)          
datatypestr{15}={1280,'UINT64',     64}; % unsigned long long (64 bits) 
datatypestr{16}={1536,'FLOAT128',   128}; % long double (128 bits)       
datatypestr{17}={1792,'COMPLEX128', 128}; % double pair (128 bits)       
datatypestr{18}={2048,'COMPLEX256', 256}; % long double pair (256 bits)  
datatypestr{19}={2304,'RGBA32', 32}; % 4 byte RGBA (32 bits/voxel) 
info.DataTypeStr='UNKNOWN';
info.BitVoxel=0;
for i=1:19
    if(datatypestr{i}{1}==info.DataType)
        info.DataTypeStr=datatypestr{i}{2};
        info.BitVoxel=datatypestr{i}{3};
    end
end

info.Bitpix=fread(fid,1,'uint16');
info.SliceStart=fread(fid,1,'uint16');
temp=fread(fid,1,'float');
info.PixelDimensions=fread(fid,7,'float');

info.VoxOffset=fread(fid,1,'float');
info.RescaleSlope=fread(fid,1,'float');
info.RescaleIntercept=fread(fid,1,'float');
info.SliceEnd=fread(fid,1,'uint16');
info.SliceCode=fread(fid, 1, 'uint8=>char')';
info.XyztUnits=fread(fid, 1, 'uint8')';
dataunitsstr{1}={'UNKNOWN', 0}; %! NIFTI code for unspecified units. 
dataunitsstr{2}={'METER',   1};  %! NIFTI code for meters. 
dataunitsstr{3}={'MM',    2};  %! NIFTI code for millimeters. 
dataunitsstr{4}={'MICRON ', 3};  %! NIFTI code for micrometers. 
dataunitsstr{5}={'SEC',    8};  %! NIFTI code for seconds. 
dataunitsstr{6}={'MSEC',   16};  %! NIFTI code for milliseconds. 
dataunitsstr{7}={'USEC',  24};  %! NIFTI code for microseconds. 
dataunitsstr{8}={'HZ',  32};  %! NIFTI code for Hertz. 
dataunitsstr{9}={'PPM',  40};  %! NIFTI code for ppm. 
dataunitsstr{10}={'RADS',  48};  %! NIFTI code for radians per second. 
info.XyztUnitsStr=sprintf('UNKNOWN [%i]', info.XyztUnits);
for i=1:10,
    if(dataunitsstr{i}{2}==info.XyztUnits)
        info.XyztUnitsStr=dataunitsstr{i}{1};
    end
end

info.CalMax=fread(fid,1,'float');
info.CalMin=fread(fid,1,'float');
info.Slice_duration=fread(fid,1,'float');
info.Toffset=fread(fid,1,'float');
info.Glmax=fread(fid,1,'int');
info.Glmin=fread(fid,1,'int');
info.Descrip=strtrim(fread(fid, 80, 'uint8=>char')');
info.AuxFile=strtrim(fread(fid, 24, 'uint8=>char')');
info.QformCode=fread(fid,1,'uint16');
info.SformCode=fread(fid,1,'uint16');
info.QuaternB=fread(fid,1,'float');
info.QuaternC=fread(fid,1,'float');
info.QuaternD=fread(fid,1,'float');
info.QoffsetX=fread(fid,1,'float');
info.QoffsetY=fread(fid,1,'float');
info.QoffsetZ=fread(fid,1,'float');
info.SrowX=fread(fid,4,'float');
info.SrowY=fread(fid,4,'float');
info.SrowZ=fread(fid,4,'float');
info.IntentName=strtrim(fread(fid, 16, 'uint8=>char')');
info.Magic=strtrim(fread(fid, 4, 'uint8=>char')');
info=orderfields(info);

fclose(fid);
if ~strncmp(info.Magic, 'nii', 3) && ~strncmp(info.Magic, 'n+1', 3)
  error([ mfilename ': ' filename ' is probably not a NifTI volume file.' ])
end


