function [Descr, args, header] = private_load_tiff(stack_name)
% PRIVATE_LOAD_TIFF Read a TIFF image stack (uncompressed)
%
%  Read a TIFF image stack.
%
% Reference:
%   https://www.itu.int/itudoc/itu-t/com16/tiff-fx/docs/tiff6.pdf
%   https://en.wikipedia.org/wiki/TIFF

%get data block size
info1           = imfinfo(stack_name);
Nframes         = numel(info1);
header = cell(1,Nframes); Descr = header; args = header;

% create entries for all images (IFD)
for index=1:Nframes
  H = info1(index);
  if isfield(H,'Compression') && ~strcmp(H.Compression, 'Uncompressed')
    disp([ mfilename ': compressed TIFF are not supported: ' filename ]);
    return
  end

  if index==1 % only get the offset info from the 1st (faster)
    stripOffset     = H.StripOffsets;
    stripByteCounts = H.StripByteCounts;
    offsets         = stripOffset(1) + (0:(Nframes-1)).*stripByteCounts + 1;
  end
  H.offset        = offsets(index);
  H.frame         = index;   

  if H.BitDepth==32
      H.DataTypeStr='uint32';
  elseif H.BitDepth==16
      H.DataTypeStr='uint16';
  else
      H.DataTypeStr='uint8';
  end

  if isfield(H,'SamplesPerPixel') && H.SamplesPerPixel > 1
    H.Dimensions = [ H.Width H.Height H.SamplesPerPixel ];
  else
    H.Dimensions = [ H.Width H.Height ];
  end

  if strcmp(H.ByteOrder, 'little-endian')
    H.MachineFormat = 'ieee-le';
  else
    H.MachineFormat = 'ieee-be';
  end

  D = 'TIFF image';

  A = {
    'Offset',         H.offset, ...
    'MachineFormat',  H.MachineFormat, ...
    'Format',         H.DataTypeStr, ...
    'Dimension' ,     H.Dimensions };
  
  % store
  Descr{index} = D; header{index} = H; args{index} = A;
end
if Nframes == 1
  Descr = Descr{1}; header = header{1}; args = args{1};
end
