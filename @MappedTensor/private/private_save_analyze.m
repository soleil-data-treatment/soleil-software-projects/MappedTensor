function count = private_save_analyze(mtVar, filename)
% PRIVATE_SAVE_ANALYZE Save an Analyze 7.5 Mayo Clinic file
%  count = private_save_analyze(mtVar, filename)
%
% Format definition and consortium <http://nifti.nimh.nih.gov/>
% Format definition <https://web.archive.org/web/20070927191351/http://www.mayo.edu/bir/PDF/ANALYZE75.pdf>
%
% Input:  filename: HDR/IMG file (string)
% output: structure

count = 0;

dim = size(mtVar);
if (length(dim) == 3), dim(4)=1; end;
if numel(dim) ~= 4
  error([ mfilename ': can only export 3D/4D data, currently ' mat2str(dim) ]);
end
siz = dim(1:3);
switch mtVar.Format
case 'uint8'
   datatype = 2; bitpix = 8;
case 'int16'
   datatype = 4; bitpix = 16;
case 'int32'
   datatype = 8; bitpix = 32;
case 'single'
   if isreal(mtVar)
      datatype = 16; bitpix = 32;
   else
      datatype = 32; bitpix = 64;
   end
case 'double'
   if isreal(mtVar)
      datatype = 64; bitpix = 64;
   else
      datatype = 1792; bitpix = 128;
   end
case 'int8'
   datatype = 256; bitpix = 8;
case 'uint16'
   datatype = 512; bitpix = 16;
case 'uint32'
   datatype = 768; bitpix = 32;
otherwise
   error([ mfilename ': Datatype is not supported: ' mtVar.Format ]);
end
lim_min = min(mtVar);
lim_max = max(mtVar);
lim = [ min(lim_min(:)) max(lim_max(:)) ]; 

% ------------------------------------------------------------------------------
% write the header
[p,f,e]  = fileparts(filename);
filename = fullfile(p,f);
[fid,mess]=fopen([ filename '.hdr' ],'wb');
if fid==-1
  error([ mfilename ': error opening ' filename ': ' mess ]);
end
%
fwrite(fid,348,'int');
fwrite(fid,zeros(28,1),'char');
fwrite(fid,16384,'int');
fwrite(fid,zeros(2,1),'char');
fwrite(fid,'r','char');
fwrite(fid,zeros(1,1),'char');
fwrite(fid,4,'int16');
fwrite(fid,dim,'int16');
fwrite(fid,zeros(20,1),'char');
fwrite(fid,datatype,'int16');
fwrite(fid,bitpix,'int16');
fwrite(fid,zeros(6,1),'char');
fwrite(fid,siz,'float32');
fwrite(fid,zeros(16,1),'char');
fwrite(fid,0,'float32');
fwrite(fid,1,'float32');
fwrite(fid,zeros(24,1),'char');
fwrite(fid,lim,'int');

descr = mtVar.Name;
descr(80)=0;
fwrite(fid,sprintf('%-80s',descr(1:80)),'char');

fwrite(fid,zeros(24,1),'char');
fwrite(fid,0,'char');  % orientation
fwrite(fid,[ 0 0 0 ],'int16');  

fwrite(fid,zeros(89,1),'char'); 

fclose(fid);

% ------------------------------------------------------------------------------
% write data: fwrite(fid, Data, mtVar.Format);

[fid,mess]=fopen([ filename '.img' ],'wb');
if fid==-1
  error([ mfilename ': error opening ' filename ': ' mess ]);
end

% we write the data by chunks
  chunkSize   = 100e6;
  elements    = prod(size(mtVar));
  i1    = 1;
  
  fprintf(1, '[%s]: ', filename);
  while i1 <= elements
    fprintf(1, '.');
    i2    = min(i1+(chunkSize-1), elements);
    chunk = subsref(mtVar, substruct('()', {i1:i2} ));
    count = count + fwrite(fid, chunk, mtVar.Format);
    i1    = i1+chunkSize;

  end
  fprintf(1, ' [%i %s]\n', count, mtVar.Format);

  fclose(fid);


