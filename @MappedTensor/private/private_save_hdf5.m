function count = private_save_hdf5(mtVar, filename, dataset, varargin)
% PRIVATE_SAVE_HDF5 Save an HDF5 file
%  count = private_save_hdf5(mtVar, filename)
%
% Format definition and consortium <http://nifti.nimh.nih.gov/https://en.wikipedia.org/wiki/Hierarchical_Data_Format>
%
% Input:  filename: HDF5 filename (string)
% output: structure

count = 0;
if isempty(dataset), dataset = '/Data'; end

% initiate the file with a new MappedTensor in HDF5 storage
mtNewVar                = MappedTensor;
mtNewVar.vnOriginalSize = size(mtVar);
mtNewVar.Filename       = filename;
mtNewVar.H5_dataset     = dataset;
mtNewVar.Writable       = true;
mtNewVar.Temporary      = false;
mtNewVar.Format         = mtVar.Format;
mtNewVar.Name           = mtVar.Name;
h5_open(mtNewVar);

% write data: fwrite(fid, Data, mtVar.Format);

% we write the data by chunks
  chunkSize   = 100e6;
  elements    = prod(size(mtVar));
  i1    = 1;
  
  fprintf(1, '[%s]: ', filename);
  while i1 <= elements
    fprintf(1, '.');
    i2    = min(i1+(chunkSize-1), elements);
    chunk = subsref(mtVar, substruct('()', {i1:i2} ));
    subsasgn(mtNewVar, substruct('()', {i1:i2} ), chunk);
    
    count = count + numel(chunk);
    i1    = i1+chunkSize-1;

  end
  close(mtNewVar);
  fprintf(1, ' [%i %s]\n', count, mtVar.Format);
  

