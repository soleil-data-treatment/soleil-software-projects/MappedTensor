function count = private_save_nii(mtVar, filename)
% PRIVATE_SAVE_NII Save a NifTi Neuroimaging Informatics Technology Initiative volume
%  count = private_save_nii(mtVar, filename)
%
% Format definition and consortium <http://nifti.nimh.nih.gov/>
%
% NifTi example data NII files at <https://nifti.nimh.nih.gov/nifti-1/data>
%
% Input:  filename: NifTi file (string)
% output: structure

% format definition:
%   https://nifti.nimh.nih.gov/pub/dist/src/niftilib/nifti1.h

count = 0;

[fid,mess] = fopen(filename,'w');
if fid==-1
  error([ mfilename ': error opening ' filename ': ' mess ]);
end

%                        /*************************/  /************************/
%struct nifti_1_header { /* NIFTI-1 usage         */  /* ANALYZE 7.5 field(s) */
%                        /*************************/  /************************/
%                                           /*--- was header_key substruct ---*/
% int   sizeof_hdr;    /*!< MUST be 348           */  /* int sizeof_hdr;      */
  fwrite(fid, 348,    'int32');	% must be 348.
% char  data_type[10]; /*!< ++UNUSED++            */  /* char data_type[10];  */
  fwrite(fid, char(zeros(1,10)), 'uchar');
% char  db_name[18];   /*!< ++UNUSED++            */  /* char db_name[18];    */
  fwrite(fid, char(zeros(1,18)), 'uchar');
% int   extents;       /*!< ++UNUSED++            */  /* int extents;         */
  fwrite(fid, 0, 'int32');
% short session_error; /*!< ++UNUSED++            */  /* short session_error; */
  fwrite(fid, 0, 'uint16');
% char  regular;       /*!< ++UNUSED++            */  /* char regular;        */
  fwrite(fid, 0, 'char');
% char  dim_info;      /*!< MRI slice ordering.   */  /* char hkey_un0;       */
  fwrite(fid, 0, 'char');
  
%                                      /*--- was image_dimension substruct ---*/
% short dim[8];        /*!< Data array dimensions.*/  /* short dim[8];        */
  dims = size(mtVar);
  dims = [length(dims) dims ones(1,8)];
  fwrite(fid, dims(1:8),        'uint16');
% float intent_p1 ;    /*!< 1st intent parameter. */  /* short unused8;       */
%                                                     /* short unused9;       */
  fwrite(fid, 0, 'float32');
% float intent_p2 ;    /*!< 2nd intent parameter. */  /* short unused10;      */
%                                                     /* short unused11;      */
  fwrite(fid, 0, 'float32');
% float intent_p3 ;    /*!< 3rd intent parameter. */  /* short unused12;      */
%                                                     /* short unused13;      */
  fwrite(fid, 0, 'float32');
% short intent_code ;  /*!< NIFTI_INTENT_* code.  */  /* short unused14;      */
  fwrite(fid, 0, 'uint16');
  
% short datatype;      /*!< Defines data type!    */  /* short datatype;      */
  switch mtVar.Format
  case 'uint8'
     datatype = 2; bitpix = 8;
  case 'int16'
     datatype = 4; bitpix = 16;
  case 'int32'
     datatype = 8; bitpix = 32;
  case 'single'
     if isreal(mtVar)
        datatype = 16; bitpix = 32;
     else
        datatype = 32; bitpix = 64;
     end
  case 'double'
     if isreal(mtVar)
        datatype = 64; bitpix = 64;
     else
        datatype = 1792; bitpix = 128;
     end
  case 'int8'
     datatype = 256; bitpix = 8;
  case 'uint16'
     datatype = 512; bitpix = 16;
  case 'uint32'
     datatype = 768; bitpix = 32;
  otherwise
     error([ mfilename ': Datatype is not supported.' ]);
  end
  fwrite(fid, datatype,     'int16');
  
% short bitpix;        /*!< Number bits/voxel.    */  /* short bitpix;        */
  fwrite(fid, bitpix,       'uint16');
% short slice_start;   /*!< First slice index.    */  /* short dim_un0;       */
  fwrite(fid, 0,       'int16');
% float pixdim[8];     /*!< Grid spacings.        */  /* float pixdim[8];     */
  fwrite(fid, [ 0 ones(1,7) ],   'float32');
% float vox_offset;    /*!< Offset into .nii file */  /* float vox_offset;    */
  fwrite(fid, 352,   'float32');
% float scl_slope ;    /*!< Data scaling: slope.  */  /* float funused1;      */
  fwrite(fid, 0,   'float32');
% float scl_inter ;    /*!< Data scaling: offset. */  /* float funused2;      */
  fwrite(fid, 0,   'float32');
% short slice_end;     /*!< Last slice index.     */  /* float funused3;      */
  fwrite(fid, 0,   'int16');
% char  slice_code ;   /*!< Slice timing order.   */
  fwrite(fid, 0,   'uchar');
% char  xyzt_units ;   /*!< Units of pixdim[1..4] */
  fwrite(fid, 0,   'uchar');
% float cal_max;       /*!< Max display intensity */  /* float cal_max;       */
  cal_max = max(mtVar); cal_max=max(cal_max(:));
  fwrite(fid, cal_max,    'float32');
% float cal_min;       /*!< Min display intensity */  /* float cal_min;       */
  cal_min = min(mtVar); cal_min=min(cal_min(:));
  fwrite(fid, cal_min,    'float32');
% float slice_duration;/*!< Time for 1 slice.     */  /* float compressed;    */
  fwrite(fid, 0,    'float32');
% float toffset;       /*!< Time axis shift.      */  /* float verified;      */
  fwrite(fid, 0,    'float32');
% int   glmax;         /*!< ++UNUSED++            */  /* int glmax;           */
  fwrite(fid, 0,    'int32');
% int   glmin;         /*!< ++UNUSED++            */  /* int glmin;           */
  fwrite(fid, 0,    'int32');
  
%                                         /*--- was data_history substruct ---*/
% char  descrip[80];   /*!< any text you like.    */  /* char descrip[80];    */
  descrip =  mtVar.Name;
  descr(80)=0;
  fwrite(fid, descrip(1:80), 'uchar');
% char  aux_file[24];  /*!< auxiliary filename.   */  /* char aux_file[24];   */
  aux_file = [ 'none' char(zeros(1,80)) ];
  fwrite(fid, aux_file(1:24), 'uchar');
% short qform_code ;   /*!< NIFTI_XFORM_* code.   */  /*-- all ANALYZE 7.5 ---*/
  fwrite(fid, 1, 'uint16');
% short sform_code ;   /*!< NIFTI_XFORM_* code.   */  /*   fields below here  */
%                                                     /*   are replaced       */
  fwrite(fid, 0, 'uint16');
% float quatern_b ;    /*!< Quaternion b param.   */
  fwrite(fid, 0, 'float32');
% float quatern_c ;    /*!< Quaternion c param.   */
  fwrite(fid, 1, 'float32');
% float quatern_d ;    /*!< Quaternion d param.   */
  fwrite(fid, 0, 'float32');
% float qoffset_x ;    /*!< Quaternion x shift.   */
  fwrite(fid, 0, 'float32');
% float qoffset_y ;    /*!< Quaternion y shift.   */
  fwrite(fid, 0, 'float32');
% float qoffset_z ;    /*!< Quaternion z shift.   */
  fwrite(fid, 0, 'float32');

% float srow_x[4] ;    /*!< 1st row affine transform.   */
  fwrite(fid, [0 0 0 0], 'float32');
% float srow_y[4] ;    /*!< 2nd row affine transform.   */
  fwrite(fid, [0 0 0 0], 'float32');
% float srow_z[4] ;    /*!< 3rd row affine transform.   */
  fwrite(fid, [0 0 0 0], 'float32');
% char intent_name[16];/*!< 'name' or meaning of data.  */
  [p,f,e] = fileparts(mtVar.Filename);
  intent_name = [ f char(zeros(1,16)) ];
  fwrite(fid, intent_name(1:16), 'uchar');
% char magic[4] ;      /*!< MUST be "ni1\0" or "n+1\0". */
  magic = [ 'n+1' char(0) ];
  fwrite(fid, magic, 'uchar');
%} ;                   /**** 348 bytes total ****/

% write data: fwrite(fid, Data, mtVar.Format);

% we write the data by chunks
  chunkSize   = 100e6;
  elements    = prod(size(mtVar));
  i1    = 1;
  
  fprintf(1, '[%s]: ', filename);
  while i1 <= elements
    fprintf(1, '.');
    i2    = min(i1+(chunkSize-1), elements);
    chunk = subsref(mtVar, substruct('()', {i1:i2} ));
    count = count + fwrite(fid, chunk, mtVar.Format);
    i1    = i1+chunkSize;

  end
  fprintf(1, ' [%i %s]\n', count, mtVar.Format);

  fclose(fid);


