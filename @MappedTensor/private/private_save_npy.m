function count = private_save_npy(mtVar, filename)
% PRIVATE_LOAD_NPY Save a Nearly Raw Raster Data
%  data = private_save_npy(mtVar, filename)
%
% Input:  filename: NPY file (string)
% output: structure

  count = 0;

  [fid,mess] = fopen(filename,'w');
  if fid==-1
    error([ mfilename ': error opening ' filename ': ' mess ]);
  end

  header = constructNPYheader(mtVar.Format, size(mtVar));
  fwrite(fid, header, 'uint8');

% we write the data by chunks
  chunkSize   = 100e6;
  elements    = prod(size(mtVar));
  i1    = 1;
  
  fprintf(1, '[%s]: ', filename);
  while i1 <= elements
    fprintf(1, '.');
    i2    = min(i1+(chunkSize-1), elements);
    chunk = subsref(mtVar, substruct('()', {i1:i2} ));
    count = count + fwrite(fid, chunk, mtVar.Format);
    i1    = i1+chunkSize;

  end
  fprintf(1, ' [%i %s]\n', count, mtVar.Format);

  fclose(fid);


