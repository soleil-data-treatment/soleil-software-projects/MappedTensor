function count = private_save_nrrd(mtVar, filename)
% PRIVATE_LOAD_NRRD Save a Nearly Raw Raster Data
%  data = private_save_nrrd(mtVar, filename)
%
% Format definition http://teem.sourceforge.net/nrrd/format.html
%
% Input:  filename: NRRD file (string)
% output: structure

count = 0;

[fid,mess] = fopen(filename,'w');
if fid==-1
  error([ mfilename ': error opening ' filename ': ' mess ]);
end


  fprintf( fid, 'NRRD0005\n' );  
  fprintf( fid, '# Complete NRRD file format specification at:\n' );
  fprintf( fid, '# http://teem.sourceforge.net/nrrd/format.html\n' );
  
  content = [ mtVar.Filename ' [' strtrim(sprintf(' %d', size(mtVar))) '] ' mtVar.Format ' array' ];
  fprintf( fid, 'content: %s\n', content);  % always optional

  type = mtVar.Format;
  if strcmp(type, 'single')
    type = 'float';
  end
  
  fprintf( fid, 'type: %s\n', type );
  fprintf( fid, 'encoding: %s\n', 'raw');
  fprintf( fid, 'dimension: %d\n', length(size(mtVar)) );
  fprintf( fid, 'sizes: ' );
  fprintf( fid, '%i ', size(mtVar));
  fprintf( fid, '\n' );
  [~,~,endian] = computer();
  if (isequal(endian, 'B'))
      endian = 'big';
  else
      endian = 'little';
  end
  fprintf( fid, 'endian: %s\n', endian );
  
  % After the header, there is a single blank line containing zero
  % characters to separate it from data
  fprintf(fid, '\n');

% we write the data by chunks
  chunkSize   = 100e6;
  elements    = prod(size(mtVar));
  i1    = 1;
  
  fprintf(1, '[%s]: ', filename);
  while i1 <= elements
    fprintf(1, '.');
    i2    = min(i1+(chunkSize-1), elements);
    chunk = subsref(mtVar, substruct('()', {i1:i2} ));
    count = count + fwrite(fid, chunk, mtVar.Format);
    i1    = i1+chunkSize;

  end
  fprintf(1, ' [%i %s]\n', count, mtVar.Format);

  fclose(fid);


