function count = save(mtVar, filename, varargin)
% SAVE Save a MappedTensor into a formatted file.
%   SAVE(M, 'file.ext') saves the disk-mapped dataset into a file, and returns 
%   the number of written elements. 
%   Complex tensors are not handled (only the real part is exported).
%
%   Supported formats:
%    | Extension         | Description                                  |
%    |-------------------|----------------------------------------------|
%    | HDF5              | Hierarchical Data Format (nD)                |
%    | IMG+HDR           | Analyze 7.5 Mayo Clinic (3D/4D)              |
%    | NII               | NifTI-1 MRI volume data (nD)                 |
%    | NPY               | Python NumPy array (nD)                      |
%    | NRRD              | Nearly Raw Raster Data (nD)                  |

[tf, filename, dataset] = mt_ishdf5(filename);
[p,f,e] = fileparts(filename);
count = 0;
switch lower(e)
case '.nrrd'
  count = private_save_nrrd(mtVar, filename);
case '.npy'
  count = private_save_npy(mtVar, filename);
case '.nii'
  count = private_save_nii(mtVar, filename);
case {'.img','.hdr'}
  count = private_save_analyze(mtVar, filename);
case {'.h5','.hdf5','.hdf'}
  count = private_save_hdf5(mtVar, filename, dataset, varargin{:});
otherwise
  error([ mfilename ': unsupported format ' e ]);
end

