function [varargout] = size(mtVar, vnDimensions)
% SIZE   Get original tensor size, and extend dimensions if necessary
%   D = SIZE(X) gets the tensor dimension. The dimension D is a vector.
%
%   [M,N,..] = SIZE(X) returns the sizes of the first N dimensions of the
%   array X.
%
%   M = SIZE(X,DIM) returns the length of the dimension specified
%   by the scalar DIM.
%
%   M = SIZE([A B C]) returns the length of all tensor sizes as a cell.
%   When all tensors A B C... have the same size, the return value is the size
%   of the equivalent stacked-tensor, returned as [M N ...].
%
%   Example: m=MappedTensor(eye(5)); size(m,1) == 5

  if numel(mtVar) > 1
    dim = numel(find(builtin('size', mtVar) > 1));
    if nargin > 1
      s = arrayfun(mtVar, 'size', vnDimensions);
    else
      s = arrayfun(mtVar, 'size');
      % test if all tensors have the same size. Compute stacked-tensor size.
      tf = cellfun(@(c)subsref_allsz(c, s{1}), s);
      tr = cellfun(@(c)subsref_allsz(c, swap(s{1})), s);
      if all(tf | tr)
        sz = builtin('size', mtVar);
        s  = [ sz(find(sz>1)) s{1} ];
        index = find(tr & ~tf);
        if ~isempty(index)
          mtVar(index) = transpose(mtVar(index));
          if mt_verbosity>0
            disp([ mfilename ': INFO: transposing ' num2str(numel(index)) ' objects from stack to align sizes.' ]);
          end
        end
      end
    end
    varargout{1} = s;
    return
  end

  vnOriginalSize = mtVar.vnOriginalSize; %#ok<PROP>
  vnOriginalSize(end+1:numel(mtVar.vnDimensionOrder)) = 1; %#ok<PROP>

  % - Return the size of the tensor data element, permuted
  vnSize = vnOriginalSize(mtVar.vnDimensionOrder); %#ok<PROP>

  % - Return specific dimension(s)
  if nargin > 1
    if (~isnumeric(vnDimensions) || ~all(isreal(vnDimensions)))
       error('MappedTensor:dimensionMustBePositiveInteger', ...
          '*** MappedTensor: Dimensions argument must be a positive integer within indexing range.');
    end

    % - Return the specified dimension(s)
    vnSize(end+1:max(vnDimensions)) = 1;
    vnSize = vnSize(vnDimensions);
  end

  % - Handle differing number of size dimensions and number of output
  % arguments
  nNumArgout = max(1, nargout);

  if (nNumArgout == 1)
    % - Single return argument -- return entire size vector
    varargout{1} = vnSize;
    
  elseif (nNumArgout <= numel(vnSize))
    % - Several return arguments -- return single size vector elements,
    % with the remaining elements grouped in the last value
    varargout(1:nNumArgout-1) = num2cell(vnSize(1:nNumArgout-1));
    varargout{nNumArgout} = prod(vnSize(nNumArgout:end));
    
  else
    % - Output all size elements
    varargout(1:numel(vnSize)) = num2cell(vnSize);

    % - Deal out trailing dimensions as '1'
    varargout(numel(vnSize)+1:nNumArgout) = {1};
  end
end

% ----------------------------------------------
function tf = subsref_allsz(c, sz)
  if numel(c) ~= numel(sz)
    tf = false;
    return
  else
    if ~isscalar(c)
      d = c([ 2:-1:1 3:end ]);
      % look if dataset is to be transposed
      tr = all(d == sz);
    else tr = false; end
    tf = all(c == sz) | tr;
  end
end

function sz=swap(sz)
  if ~isscalar(sz)
    sz = sz([ 2:-1:1 3:end ]);
  end
end
