function v = subsref(mtVar, S)
% SUBSREF Subscripted reference.
%   The subscript reference can be applied with syntax:
%   M(I) which referes to element I in array M. For multi-dimensional arrays,
%   the syntax is M(I,J,...). The use of ':' in place an index indicates the
%   whole dimension.
%
%   The special syntax M(:) extracts all values of the tensor.
%
%   When M is itself an array of tensors (stack), the syntax M(I) extracts 
%   elements out of the meta-tensor, whereas M{I} extracts tensor I out of M.
%
%   M.field accesses the property 'field' in object M.
%
% Example: % Example: m=MappedTensor(eye(5)); subsref(m,1) == 1 % same as m(1)

% HDF5 handling calls h5_getdata, and does not call the hShimFunc

  if ischar(S) 
    S = substruct('.',S);
  elseif isnumeric(S)
    S = substruct('()',{ S });
  end

  % handle array of objects
  if numel(mtVar) > 1
    v = [];
    switch S(1).type
    case {'()','{}'}
      sz  = size(mtVar); % meta tensor size
      dim = numel(find(sz > 1));
      if numel(S(1).subs) == 1 && strcmp(S(1).type, '()')
        % array(i1:i2) uses ind2sub, only for () indexing
        varg=cell(1,dim);
        [varg{:}] = ind2sub(size(mtVar), S(1).subs{:});
        for i=1:dim	% handle singleton indices
          this = varg{i};
          if all(this == this(1)), varg{i}=this(1); end
        end
        S(1).subs = varg;
        linear_indexing = 1;
      else linear_indexing = 0;
      end
      S(1).type = '()';

      dim = numel(find(builtin('size', mtVar) > 1));
      % we assume first indices are for the array, up to its dimensionality
      % further indices are passed to each object...
      if numel(S(1).subs) <= dim
        v = builtin('subsref', mtVar, S);
      else 
        % handle mixed stack/inner object referencing
        
        % we have further dimensions than the array of object.
        % we split subs into the indices up to dim for the array of objects
        mtVar = builtin('subsref', mtVar, substruct('()', S(1).subs(1:dim)));
        % when in event-style mtvar has as many elements as the number of indices to extract
        
        % and the rest for each object.
        v = cell(1,numel(mtVar));
        for index=1:numel(mtVar)
          subs = S(1).subs((dim+1):end);
          if linear_indexing
            for i=1:numel(subs)
              if ~isscalar(subs{i}), s=subs{i}; s=s(index); subs{i}=s; end
            end
          end
          v{index} = subsref(mtVar(index), substruct('()', subs));
        end
        v = reshape(v, builtin('size', mtVar)); % size of initial array of objects (like a cell)
        % when all results are same size, we convert to a single matrix
        sz = cellfun(@size, v, 'UniformOutput', false);
        tf = cellfun(@(c)subsref_allsz(c, sz{1}), sz);
        if all(tf)
          % we wish to put the indices for the array of object first
          % numel(v)+1 -> 1
          n = numel(v);
          v = cat(n+1, v{:}); v = shiftdim(v, n);
        end
      end
      return
      
    otherwise
      v = {};
      for index=1:numel(mtVar)
        v{end+1} = subsref(mtVar(index), S);
      end
      return
    end
  end
  
  % - Check reference type
  switch (S(1).type)
  case {'()'}
    % - Call the internal subsref function to access data
    tf = mt_ishdf5(mtVar);
    if tf == 1       % HDF5
      v = h5_getdata(mtVar, S(1).subs{:});
    elseif tf == 2   % MAT
      mat_open(mtVar);
      sz  = size(mtVar);
      dim = numel(find(sz > 1));
      if numel(S(1).subs) == 1 && dim > 1
        varg=cell(1,dim);
        [varg{:}] = ind2sub(sz, S(1).subs{:});
        for i=1:dim
          this = varg{i};
          if all(this == this(1)), varg{i}=this(1); end
        end
        S(1).subs = varg;
      end
      v = subsref(mtVar.hRealContent, [ substruct('.', mtVar.MAT_dataset) S ]);
    else
      v = mt_subsref(mtVar, S);
    end
  otherwise
    v = builtin('subsref', mtVar, S);
  end
end

% ----------------------------------------------
function tf = subsref_allsz(c, sz)
  if numel(c) ~= numel(sz)
    tf = false;
    return
  else
    tf = all(c == sz);
  end
end
